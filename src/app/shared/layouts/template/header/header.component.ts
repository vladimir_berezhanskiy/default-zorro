import {Component, Input, OnInit, Output, ViewChild} from '@angular/core';
import {SidenavService} from "../../../services/sidenav.service";
import {ChartComponent} from "ng-apexcharts";
import {AuthenticationService} from "../../../services/authentication.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  avatarColor: string = '';
  userValue: string;

  @ViewChild('chart', {static: true}) public chart: ChartComponent;
  @Input() isCollapsed: boolean = false;

  constructor(private navService: SidenavService, private authServise: AuthenticationService) {
  }

  ngOnInit(): void {
    this.navService.isMenuFoldedChanges.subscribe(isCollapsed => this.isCollapsed = isCollapsed);
    this.userValue = this.authServise.currentUserValue.email;
    this.avatarColor = this.randomAvatar();
  }

  toggleFold() {
    this.isCollapsed = !this.isCollapsed;
    this.navService.toggleFold(this.isCollapsed);
  }

  randomAvatar(): string {
      const randomColor = Math.floor(Math.random() * 16777215).toString(16);
      /*console.log(randomColor)*/
      return `#${randomColor}`;
  }

  logout() {
    this.authServise.logout();
  }
}
