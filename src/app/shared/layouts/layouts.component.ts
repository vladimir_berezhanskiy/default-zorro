import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {SidenavService} from "../services/sidenav.service";
import {ActivatedRoute, NavigationEnd, Params, PRIMARY_OUTLET, Router} from "@angular/router";
import {filter} from "rxjs/operators";
import {BreadcrumbOption} from "ng-zorro-antd/breadcrumb";
import {ChartComponent} from "ng-apexcharts";
import {LoadingService} from "../services/loader.service";
import {Spinkit} from "ng-http-loader";

interface IBreadcrumb {
  label: string;
  params: Params;
  url: string;
}

@Component({
  selector: 'app-layouts',
  templateUrl: './layouts.component.html',
  styleUrls: ['./layouts.component.scss']
})
export class LayoutsComponent implements OnInit {
  public breadcrumbs: IBreadcrumb[] | any = [];
  spinnerStyle = Spinkit;
  opacity: string = '0.6'


  @Input() isCollapsed: boolean = false;

  constructor(private navService: SidenavService) {
    this.breadcrumbs = [];
  }

  ngOnInit(): void {
    this.navService.isMenuFoldedChanges.subscribe(isCollapsed => this.isCollapsed = isCollapsed);
  }
}
