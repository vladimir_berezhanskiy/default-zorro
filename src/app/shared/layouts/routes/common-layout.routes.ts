import {Routes} from '@angular/router';
import {AuthGuards} from "../../guards/auth.guard";

export const CommonLayout_ROUTES: Routes = [
  {
    path: 'dashboard',
    canActivate: [AuthGuards],
    loadChildren: () => import('../../../pages/dashboard/dashboard.module').then(m => m.DashboardModule),
    data: {
      breadcrumb: 'Dashboard'
    }
  },
  {
    path: 'events',
    canActivate: [AuthGuards],
    loadChildren: () => import('../../../pages/events/events.module').then(m => m.EventsModule),
    data: {
      breadcrumb: 'Events'
    }
  },
];
