import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';
import {CookieService} from "ngx-cookie-service";

@Injectable({providedIn: 'root'})
export class AuthGuards implements CanActivate {
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private cookieService: CookieService
  ) {}
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const currentUser = this.authenticationService.currentUserValue;
    const currentUserAuth = this.authenticationService.currentValue;
    const cookie = this.cookieService.check('isAuth');
    if (currentUser && currentUserAuth && cookie) {
      return true;
    }
    this.authenticationService.logout();
    return false;
  }
}
