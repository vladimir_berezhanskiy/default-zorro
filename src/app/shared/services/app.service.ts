import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {User} from "../interfaces/user.type";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})

export class AppService {
  constructor(private http: HttpClient) {}
  public getMe(): Observable<User> {
    return this.http.get<User>(`${environment.apiUrl}user/me`)
  }

}
