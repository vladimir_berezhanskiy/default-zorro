import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {BehaviorSubject, Observable, pipe, Subscriber, Subscription} from 'rxjs';
import {map} from 'rxjs/operators';

import {User} from '../interfaces/user.type';
import {environment} from "../../../environments/environment";
import {CookieService} from "ngx-cookie-service";
import {Router} from "@angular/router";

const USER_AUTH_API_URL = '';
const options = {
  headers: new HttpHeaders({'Content-Type': 'application/json'}),
  withCredentials: true,
};

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private currentUserSubject: BehaviorSubject<User | any>;
  private isAuthSubject: BehaviorSubject<boolean | any>;
  public isAuth: Observable<boolean>
  public currentUser: Observable<User>;
  sub: Subscription;
  constructor(private http: HttpClient, private router: Router) {
    // @ts-ignore
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.isAuthSubject = new BehaviorSubject<boolean | any>(localStorage.getItem('isAuth'));
    this.currentUser = this.currentUserSubject.asObservable();
    this.isAuth = this.isAuthSubject.asObservable()
  }

  public get currentUserValue(): User {
    console.log(this.currentUserSubject.value)
    return this.currentUserSubject.value;
  }

  public get currentValue(): boolean {
    console.log(this.isAuthSubject.value)
    return this.isAuthSubject.value;
  }

  login(user: User) {
    return this.http.post<any>(`${environment.apiUrl}auth/login`, user, options)
      .pipe(map(user => {
        if (user) {
          localStorage.setItem('currentUser', JSON.stringify(user));
          localStorage.setItem('isAuth', String(true));
          console.log(user);
          this.currentUserSubject.next(user);
          this.isAuthSubject.next(true);
        }
        return user;
      }));
  }

  logout() {
    this.sub = this.http.post<any>(`${environment.apiUrl}auth/logout`, '', options).subscribe(
      () => {
        localStorage.removeItem('currentUser');
        localStorage.removeItem('isAuth');
        this.isAuthSubject.next(null);
        this.currentUserSubject.next(null);
        this.router.navigate(['/login']);
      },
      (err) => {console.log(err)
      },
      () => this.sub.unsubscribe())
  }
}
