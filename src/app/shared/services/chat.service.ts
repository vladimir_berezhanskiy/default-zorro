import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { io, Socket } from 'socket.io-client';

@Injectable({providedIn: 'root'})
export class ChatService {
  private socket: Socket;

  constructor() {
   // this.socket = io('http://localhost:3500', {transports: ['websocket']});
    this.socket = io('http://localhost:3500', {transports: ['websocket']});
  }

  // EMITTER
  sendMessage(msg: string) {
    this.socket.emit('send_message', { message: msg });
  }

  // HANDLER
  onNewMessage() {
    return new Observable(observer => {
      this.socket.on('request_all_messages', msg => {
        observer.next(msg);
      });
    });
  }
}
