import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class SidenavService {
  isMenuFolded: boolean = false
  private isMenuFoldedActived = new BehaviorSubject<boolean>(this.isMenuFolded);
  isMenuFoldedChanges: Observable<boolean> = this.isMenuFoldedActived.asObservable();

  constructor() { }

  toggleFold(isCollapsed: boolean) {
    this.isMenuFoldedActived.next(isCollapsed);
  }
}
