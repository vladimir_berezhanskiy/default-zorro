import {Injectable} from '@angular/core';
import {HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse} from '@angular/common/http';
import {finalize, Observable, of} from 'rxjs';
import {catchError} from 'rxjs/operators';

import {AuthenticationService} from '../services/authentication.service';
import {NzNotificationService} from "ng-zorro-antd/notification";
import {CookieService} from "ngx-cookie-service";

@Injectable({providedIn: 'root'})
export class JwtInterceptor implements HttpInterceptor {
  private handleAuthError(err: HttpErrorResponse): Observable<any> {
    const cookie = this.cookieService.check('isAuth');
    this.createNotification('warning', `Error: ${err.status}`,  err?.error?.message);
    if (err.status != 429 && cookie) {
      this.authenticationService.logout();
    }
    return of(err);
  }

  constructor(private authenticationService: AuthenticationService, private notification: NzNotificationService, private cookieService: CookieService) {
  }
  createNotification(type: string, title: any, content: any): void {
    this.notification.create(
      type,
      title,
      content
    );
  }

  intercept(request: HttpRequest<any | unknown> , next: HttpHandler): Observable<HttpEvent<any | unknown>> {
    const currentUser = this.authenticationService.currentUserValue;
    const currentUserAuth = this.authenticationService.currentValue;
    const cookie = this.cookieService.check('isAuth');
    if (currentUser && currentUserAuth && cookie) {
      request = request.clone({
        withCredentials: true,
      });
    }

    return next.handle(request).pipe(
      catchError(err => this.handleAuthError(err)),
/*      finalize(() => {

      }),*/
    );
  }
}
