import {
  AfterContentChecked,
  AfterContentInit,
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit
} from '@angular/core';
import {LoadingService} from "./shared/services/loader.service";
import {Spinkit} from "ng-http-loader";
import {LayoutsComponent} from "./shared/layouts/layouts.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterContentInit, AfterContentChecked {
  constructor(private cdref: ChangeDetectorRef) {
  }

  spinnerStyle = Spinkit;
  opacity: string = '0.6'
  public awesomeComponent = LayoutsComponent;


  ngAfterContentInit(): void {
  }

  ngOnInit(): void {
    /*this.isSpinning = false*/
  }

  ngAfterContentChecked(): void {
    this.cdref.detectChanges();
  }
}
