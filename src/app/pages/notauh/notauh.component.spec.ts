import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NotauhComponent } from './notauh.component';

describe('NotauhComponent', () => {
  let component: NotauhComponent;
  let fixture: ComponentFixture<NotauhComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NotauhComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NotauhComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
