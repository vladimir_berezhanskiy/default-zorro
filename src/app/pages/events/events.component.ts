import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss']
})
export class EventsComponent implements OnInit {
  loading = true;
  cards: Array<any> = [
    {id:0,name:"Angular"},
    {id:1,name:"Typescript"},
    {id:2,name:"Javascript"},
    {id:3,name:"HTML"},
    {id:4,name:"Java"},
    {id:5,name:"dotnet"}];

  constructor() { }

  ngOnInit(): void {
  }

  removeItem(id: any){
    console.log(id)
    this.cards = this.cards.filter(item => item.id !== id);
    console.log(this.cards)
  }
}
