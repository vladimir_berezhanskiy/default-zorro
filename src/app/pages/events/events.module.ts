import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {EventsComponent} from "./events.component";
import {EventsRoutingModule} from "./events-routing.module";
import {NzCardModule} from "ng-zorro-antd/card";
import {NzGridModule} from "ng-zorro-antd/grid";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzLayoutModule} from "ng-zorro-antd/layout";
import {NzAvatarModule} from "ng-zorro-antd/avatar";
import {NzSkeletonModule} from "ng-zorro-antd/skeleton";
import {IconsProviderModule} from "../../icons-provider.module";
import {NzListModule} from "ng-zorro-antd/list";
import {NzStatisticModule} from "ng-zorro-antd/statistic";



@NgModule({
  declarations: [EventsComponent],
  imports: [
    CommonModule,
    EventsRoutingModule,
    NzGridModule,
    NzCardModule,
    NzButtonModule,
    NzAvatarModule,
    NzSkeletonModule,
    IconsProviderModule,
    NzListModule,
    NzStatisticModule,
  ]
})
export class EventsModule { }
