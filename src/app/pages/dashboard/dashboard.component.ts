import {AfterContentChecked, AfterContentInit, AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {
  ApexAxisChartSeries,
  ApexChart,
  ApexXAxis,
  ApexDataLabels,
  ApexTitleSubtitle,
  ApexStroke,
  ApexGrid
} from "ng-apexcharts";
import {AppService} from "../../shared/services/app.service";
import {LoadingService} from "../../shared/services/loader.service";
import {Spinkit} from "ng-http-loader";
import {ChatService} from "../../shared/services/chat.service";

export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  dataLabels: ApexDataLabels;
  grid: ApexGrid;
  stroke: ApexStroke;
  title: ApexTitleSubtitle;
};

@Component({
  selector: 'app-welcome',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit, AfterViewInit, AfterContentInit, AfterContentChecked {

  public chartOptions: Partial<ChartOptions> | any;
  data = [
    {
      title: 'Title 1'
    },
    {
      title: 'Title 2'
    },
    {
      title: 'Title 3'
    },
    {
      title: 'Title 4'
    },
    {
      title: 'Title 5'
    },
    {
      title: 'Title 6'
    }
  ];

  constructor(private appService: AppService, private chatService: ChatService) {
    this.chartOptions = {
      series: [
        {
          name: "Desktops",
          data: [10, 41, 35, 51, 49, 62, 69, 91, 148]
        }
      ],
      chart: {
        height: 350,
        width: '100%',
        type: "line",
        zoom: {
          enabled: false
        }
      },
      dataLabels: {
        enabled: true
      },
      stroke: {
        curve: "smooth",
        width: 1
      },
      title: {
        text: "Product Trends by Month",
        align: "left"
      },
      grid: {
        row: {
          colors: ["#f3f3f3", "transparent"], // takes an array which will be repeated on columns
          opacity: 0.5
        }
      },
      xaxis: {
        categories: [
          "Jan",
          "Feb",
          "Mar",
          "Apr",
          "May",
          "Jun",
          "Jul",
          "Aug",
          "Sep"
        ]
      }
    };
  }

  ngAfterContentInit(): void {
  }

  ngOnInit() {
    this.chatService.onNewMessage().subscribe((message)=> {
      console.log(message);
    })

    this.chatService.sendMessage('234234');

    this.chatService.onNewMessage().subscribe((message)=> {
      console.log(message);
    })
    this.appService.getMe().subscribe(value => {
    })
  }

  ngAfterViewInit(): void {
  }

  ngAfterContentChecked(): void {
  }

}
