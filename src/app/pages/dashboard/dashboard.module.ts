import { NgModule } from '@angular/core';

import { DashboardRoutingModule } from './dashboard-routing.module';

import { DashboardComponent } from './dashboard.component';
import {NzListModule} from "ng-zorro-antd/list";
import {NzCardModule} from "ng-zorro-antd/card";
import {NzGridModule} from "ng-zorro-antd/grid";
import {CommonModule} from "@angular/common";
import {NzStatisticModule} from "ng-zorro-antd/statistic";
import {NgApexchartsModule} from "ng-apexcharts";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {IconsProviderModule} from "../../icons-provider.module";
import {NgHttpLoaderModule} from "ng-http-loader";



@NgModule({
    imports: [FormsModule, DashboardRoutingModule, ReactiveFormsModule, NzListModule, NzCardModule, NzGridModule, CommonModule, NzStatisticModule, NgApexchartsModule, IconsProviderModule, NgHttpLoaderModule],
  declarations: [DashboardComponent],
  exports: [DashboardComponent],
})
export class DashboardModule { }
