import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthenticationService} from "../../shared/services/authentication.service";
import {User} from "../../shared/interfaces/user.type";
import {delayWhen, retryWhen, shareReplay, tap, timer} from "rxjs";
import {map} from "rxjs/operators";
import {Router} from "@angular/router";
import {CookieService} from "ngx-cookie-service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

  export class LoginComponent implements OnInit {
  validateForm!: FormGroup;

  submitForm(): void {
    for (const i in this.validateForm.controls) {
      if (this.validateForm.controls.hasOwnProperty(i)) {
        this.validateForm.controls[i].markAsDirty();
        this.validateForm.controls[i].updateValueAndValidity();
      }
    }
    const users: User = {
      email: this.validateForm.get('email')?.value,
      password: this.validateForm.get('password')?.value
    }
    console.log(users);
    this.AuthService.login(users).pipe(
      tap(() => console.log('tap')),
      map(res => {
        Object.values(res['id'])
        console.log(Object.values(res['id']))
      }),
      shareReplay(),
      retryWhen(errors => {
        return errors
          .pipe(
            delayWhen(() => timer(2000)),
            tap(() => console.log('return...'))
          );
      })
    )
      .subscribe(
        res => {
          this.router.navigate(['/dashboard']);
          console.log(res)
        },
        err => console.log(err),
        () => console.log('Complete')
      );
  }

  constructor(private fb: FormBuilder, private AuthService: AuthenticationService, private router: Router,
              private cookieService: CookieService) {}

  ngOnInit(): void {
    const currentUser = this.AuthService.currentUserValue;
    const currentUserAuth = this.AuthService.currentValue;
    const cookie = this.cookieService.check('isAuth');
    if (currentUser && currentUserAuth && cookie) {
      this.router.navigate(['/dashboard'])
    }
    this.validateForm = this.fb.group({
      email: [null, [Validators.required]],
      password: [null, [Validators.required]],
      remember: [true]
    });
  }
}
