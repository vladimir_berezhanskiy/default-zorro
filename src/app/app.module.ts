import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { NZ_I18N } from 'ng-zorro-antd/i18n';
import { en_US } from 'ng-zorro-antd/i18n';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { FormsModule } from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule, HttpClientXsrfModule} from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { IconsProviderModule } from './icons-provider.module';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import {NzBreadCrumbModule} from "ng-zorro-antd/breadcrumb";
import {NzAutocompleteModule} from "ng-zorro-antd/auto-complete";
import {NzInputModule} from "ng-zorro-antd/input";
import {CdkScrollableModule} from "@angular/cdk/scrolling";
import {NzButtonModule} from "ng-zorro-antd/button";
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { LayoutsComponent } from './shared/layouts/layouts.component';
import {TemplateModule} from "./shared/layouts/template/template.module";
import {NzResultModule} from "ng-zorro-antd/result";
import {PerfectScrollbarModule} from "ngx-perfect-scrollbar";
import {NzDividerModule} from "ng-zorro-antd/divider";
import {NzGridModule} from "ng-zorro-antd/grid";
import {JwtInterceptor} from "./shared/interceptors/token.interceptor";
import {CookieService} from "ngx-cookie-service";
import { NotauhComponent } from './pages/notauh/notauh.component';
import { HomeComponent } from './pages/home/home.component';
import {NzSpinModule} from "ng-zorro-antd/spin";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";
import {NzAlertModule} from "ng-zorro-antd/alert";
import {NzSwitchModule} from "ng-zorro-antd/switch";
import {NgHttpLoaderModule} from "ng-http-loader";



registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
    LayoutsComponent,
    NotFoundComponent,
    NotauhComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    HttpClientXsrfModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    IconsProviderModule,
    NzLayoutModule,
    NzMenuModule,
    NzBreadCrumbModule,
    NzAutocompleteModule,
    NzInputModule,
    CdkScrollableModule,
    NzButtonModule,
    TemplateModule,
    NzResultModule,
    PerfectScrollbarModule,
    NzDividerModule,
    NzGridModule,
    NzSpinModule,
    NzToolTipModule,
    NzAlertModule,
    NzSwitchModule,
    NgHttpLoaderModule.forRoot(),
  ],
  providers: [{ provide: NZ_I18N, useValue: en_US }, { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true }, CookieService],
  bootstrap: [AppComponent],
})
export class AppModule { }
