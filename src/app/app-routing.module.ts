import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NotFoundComponent} from "./pages/not-found/not-found.component";
import {LayoutsComponent} from "./shared/layouts/layouts.component";
import {CommonLayout_ROUTES} from "./shared/layouts/routes/common-layout.routes";
import {NotauhComponent} from "./pages/notauh/notauh.component";
import {HomeComponent} from "./pages/home/home.component";

const routes: Routes = [
  { path: '', pathMatch: 'full', component: HomeComponent},
  {
    path: 'login',
    loadChildren: () => import('../app/pages/login/login.module').then(m => m.LoginModule),
/*    data: {
      breadcrumb: 'Login'
    }*/
  },
  { path: '', component: LayoutsComponent, children: CommonLayout_ROUTES},
  { path: '401', pathMatch: 'full', component:  NotauhComponent},
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    anchorScrolling: 'enabled',
    scrollPositionRestoration: 'enabled'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
