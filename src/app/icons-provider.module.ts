import {NgModule} from '@angular/core';
import {NZ_ICONS, NzIconModule} from 'ng-zorro-antd/icon';

import {
  MenuFoldOutline,
  MenuUnfoldOutline,
  FormOutline,
  DashboardOutline,
  HeartTwoTone,
  ArrowDownOutline,
  ArrowUpOutline,
  OrderedListOutline,
  HomeOutline,
  InfoCircleOutline,
  LoginOutline,
  SettingOutline,
  EditOutline,
  EllipsisOutline,
  UserOutline,
  LockOutline,
  LogoutOutline,
  ProfileOutline
} from '@ant-design/icons-angular/icons';

const icons = [MenuFoldOutline, MenuUnfoldOutline, DashboardOutline, FormOutline, HeartTwoTone, ArrowDownOutline,
  ArrowUpOutline, OrderedListOutline, HomeOutline, InfoCircleOutline, LoginOutline, SettingOutline,
  EditOutline, EllipsisOutline, UserOutline, LockOutline, LogoutOutline, ProfileOutline];

@NgModule({
  imports: [NzIconModule],
  exports: [NzIconModule],
  providers: [
    {provide: NZ_ICONS, useValue: icons}
  ]
})
export class IconsProviderModule {
}
